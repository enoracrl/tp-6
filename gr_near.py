import argparse  # Pour les options en ligne de commande

#fonction qui lit les fichier bed et les renvoie sous la forme d'une liste de dictionnaire
#chaque dictionnaire a pour cle : chromosome, start, stop, nom
def read_file(file_gene, file_region):
    genes = []
    with open(file_gene, "r") as fgene:
        lignes = fgene.readlines()
        for ligne in lignes:
            line = ligne.split("\t")
            dico = {}
            dico["chromosome"] = line[0]
            dico["start"] = line[1]
            dico["stop"] = line[2]
            dico["nom"] = line[3]
            genes.append(dico)

    regions = []
    with open(file_region, "r") as fregion:
        lignes = fregion.readlines()
        for ligne in lignes:
            line = ligne.split("\t")
            dico = {}
            dico["chromosome"] = line[0]
            dico["start"] = line[1]
            dico["stop"] = line[2]
            dico["nom"] = line[3][0:-1]
            regions.append(dico)

    return genes, regions

#fonction qui verifie le format
def verif_format_bed(file_gene, file_region):
    print(file_gene[-4:])
    if file_gene[-4:] == ".bed":
        if file_region[-4:] == ".bed":
            return True
        else:
            print("Le fichier de regions n'est pas un ficher .bed")
            return False
    else:
        print("Le fichier de genes n'est pas un fichier .bed")
        return False

#fonction qui calcul les distances et renvoie une liste de dictionnaire
#Dont chaque dictionnaire contient les cle suivantes : chromosome ; gene ; region ; distance
def calc_dist(genes, regions, seuil):
    couples = []
    for gene in genes :
        for region in regions:
            # si l'identifiant du gene est le meme que l'identifiant de la region : on est sur le meme chromosome
            if gene['chromosome'] == region['chromosome'] :
                if int(gene['start']) > int(region['stop']):
                # le gene est en aval de la region
                    start = int(gene['start'])
                    stop = int(region['stop'])
                    distance = start - stop
                    dico = {}
                    dico["chromosome"] = gene['chromosome']
                    dico["gene"] = gene['nom']
                    dico["region"] = region['nom']
                    dico["distance"] = distance
                    couples.append(dico)
                elif int(gene['stop']) < int(region['start']) :
                # le gene est en amont de la region
                    start = int(region['start'])
                    stop = int(gene['stop'])
                    distance = start - stop
                    dico = {}
                    dico["chromosome"] = gene['chromosome']
                    dico["gene"] = gene['nom']
                    dico["region"] = region['nom']
                    dico["distance"] = distance
                    couples.append(dico)                
                elif int(region['start']) > int(gene['stop']) > int(region['stop']) or int(region['start']) < int(gene['start']) < int(region['stop']) :
                # le gene est dans la region
                    # la distance est nulle
                    distance = 0
    for i in couples:
        if int(i['distance']) <= seuil:
            couples.remove(i)
    return couples


def write_file(output, couples): 
    with open ("output", "w") as ofh:
              
            for couple in couples:
                s= couple["chromosome"]+ "\t"+ couple["gene"]+"\t"+ couple["region"]+ "\t"+ str(couple["distance"])+"\n"
                ofh.write(s)
genes, regions=read_file("gene1.bed", "region1.bed")
couples= calc_dist(genes,regions)
write_file("test.bed",couples)

def main():
    parser = argparse.ArgumentParser()
    # Lire les options de la ligne de commande
    parser.add_argument("inputg", help="fichier de genes")
    parser.add_argument("inputr", help="fichier de la region")
    parser.add_argument("--output", help="fichier de sortie (si non precise, la sortie sera en sortie standart")
    parser.add_argument("--s", type=int, help="Entrer la valeur seuil (si non precise 500000)")
    args = parser.parse_args()

    # Integrer les options lues dans le programme
    file_gene = args.inputg
    file_region = args.inputr
    if args.output:
        output = args.output
        std = False
    else:
        std = True
    if args.s:
        seuil = args.s
    else:
        seuil = 500000
    if verif_format_bed(file_gene, file_region) :
        #Ecriture 1
        genes, regions = read_file(file_gene,file_region)
        res = calc_dist(genes,regions, seuil)
        write_file(res,output,std)
        #Ecriture 2
        #write_file(calc_dist(read_file(file_gene,file_region)),output,std)

if __name__ == "__main__":
    main()
